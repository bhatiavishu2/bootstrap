(function () {
    function buildSidebarCategoriesTemplate({ name, count }) {
        return (`<div class="item d-flex justify-content-between">${name}
            <span>${count}</span>
        </div>`)
    }

    function buildSidebarLatestPostTemplate({ imagePath, text }) {
        return (`<a href="#" class="pb-2">
        <div class="item d-flex align-items-center">
            <div class="image">
                <img src="${imagePath}" alt="..." class="img-fluid">
            </div>
            <div class="title ml-4">
                <strong>${text}</strong>
                <div class="d-flex align-items-center">
                    <div class="views">
                        <i class="icon-eye"></i> 500</div>
                    <div class="comments">
                        <i class="icon-comment"></i>12</div>
                </div>
            </div>
        </div>
    </a>`)
    }
    const { categoriesData, resentPostsData } = blogData;

    const categories = categoriesData.map(function (category) {
        return buildSidebarCategoriesTemplate(category);
    }).join('');
    document.getElementById('categoriesContainer').innerHTML += categories;
    const recentPosts = resentPostsData.map(function (recentPost) {
        return buildSidebarLatestPostTemplate(recentPost);
    }).join('');
    document.getElementById('latestPostContainer').innerHTML += recentPosts;
    setInterval(function () {
        const imageNumber = Math.floor(Math.random() * 4) + 1;
        document.getElementById('adSenseImg').src = `images/ad${imageNumber}.png`;
    }, 1000)
})()

