(function () {
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    function validateUserName(userName) {
        var re = /^[A-Za-z]+$/;
        return re.test(String(userName).toLowerCase());
    }
    document.getElementById("btnSubmit").onclick = function (e) {
        e.preventDefault()
        const userName = document.getElementById("txtUserName").value

        if (!validateUserName(userName)) {
            document.getElementById("txtUserName").classList.add('is-invalid');
            return
        }
        const email = document.getElementById("txtEmail").value
        if (!validateEmail(email)) {
            document.getElementById("txtEmail").classList.add('is-invalid');
            return
        }
        alert(`userName:${userName}  email:${email}`)
        document.getElementById("txtEmail").value = '';
        document.getElementById("txtUserName").value = '';
        return
    }
    document.getElementById("txtUserName").addEventListener('focus', function () {
        this.classList.remove('is-invalid')
    })
    document.getElementById("txtEmail").addEventListener('focus', function () {
        this.classList.remove('is-invalid')
    })
})()