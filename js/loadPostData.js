function loadXMLDoc(cb) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status == 200) {
                cb(JSON.parse(xmlhttp.responseText));
            }
            else if (xmlhttp.status == 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("GET", "./data/data.json", true);
    xmlhttp.send();
}
function buildResentPostTemplate({ icon, title }) {
    return (`<div class="slide hidden col-md-4 text-center">
    <img src="${icon}"
    />
    <span class="mt-5">${title}</span>
</div>`)
}
loadXMLDoc(function ({ recomendedPosts }) {
    const recomendedPostsTemplate = recomendedPosts.map(post => buildResentPostTemplate(post)).join('')
    document.getElementById('slides').innerHTML += recomendedPostsTemplate;
    startSlider();
})