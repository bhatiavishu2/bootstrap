
(function () {
    // your page initialization code here
    // the DOM will be available here
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    document.getElementById("openNav").addEventListener('click', function () {
        openNav()
    })
     document.getElementById("closeNav").addEventListener('click', function () {
        closeNav()
    })
})();