function startSlider() {
    var controls = document.querySelectorAll('.controls');
    var showNoOfPhotos = 3
    for (var i = 0; i < controls.length; i++) {
        controls[i].style.display = 'inline-block';
    }

    var slides = document.querySelectorAll('#slides .slide');
    var currentSlide = 0;

    function nextSlide() {
        goToSlide(currentSlide + 1);
    }

    function previousSlide() {
        goToSlide(currentSlide - 1);
    }
    function hideAllSlides() {
        for (let i = 0; i < slides.length; i++) {
            slides[i].classList.add('hidden');
            slides[i].classList.remove('showing');
        }
    }
    function showCurrentSlide(slide) {
        slide.classList.add('showing');
        slide.classList.remove('hidden');
    }
    function goToSlide(n) {
        hideAllSlides();
        currentSlide = (n + slides.length) % slides.length;
        for (let i = 0; i < showNoOfPhotos; i++) {
            if (currentSlide + i == slides.length) {
                currentSlide = 0 - i
            }
            showCurrentSlide(slides[currentSlide + i])
        }
    }


    var next = document.getElementById('next');
    var previous = document.getElementById('previous');

    next.onclick = function () {
        nextSlide();
    };
    previous.onclick = function () {
        previousSlide();
    };
    goToSlide(currentSlide)
}